package shelly

import (
	"log"
	"os"
	"os/exec"
	"testing"
)

func TestLines(t *testing.T) {
	//lines, err := Lines("; \";\" ;;")
	//Lines("a\nb\\\\nc")
	lines, err := Lines(`
	"A STR"
	`)
	//lines, err := Lines(`
	//go build -o main.exe -ldflags="a=1 b=2" ./main.go
	//"A STR" 1 2 "a b"  1.23 \n &
	//"STR \\ with escape \n chars \t" 1 234 true
	//ls -larth
	//pwd
	//whoami
	//date`)
	if err != nil {
		t.Fatal(err)
	}

	for _, l := range lines {
		log.Printf("%#v", l)
	}
}

func TestExec(t *testing.T) {
	_, err := Exec(`
	ls -larth &
	pwd &
	whoami &
	date`, &Opts{
		Await: false,
		SetupProc: func(cmd *exec.Cmd) {
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
		},
	})
	if err != nil {
		t.Fatal(err)
	}

	log.Printf("DONE")

}
