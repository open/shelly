# Shelly

An abstraction for shell directly in GO

## Goal

Simplify process spawning and execution on top of exec.Command function.

The proposal is: use a string based approach for launching other process - as if you were calling a shell, but instead,
its go directly executing your commands.

Example:

```go

package main

import "go.digitalcircle.com.br/open/shelly"
import "os"
import "os/exec"

func main() {
	_, err := shelly.Exec(`
	ls -larth &
	pwd &
	whoami
	date`,
		&shelly.Opts{
			Await: false,
			Wd:    ".",
			Debug: true,
			SetupProc: func(cmd *exec.Cmd) {
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
			},
		})
	if err != nil {
		panic(err)
	}
}
```

Please note: Commands may be connected by `;` and `\n`. In any case they will be executed sequentially. Lines terminated
by `&` will be executed in parallel.

In case the Await property is true, shelly will wait for all lines to be executed (including parallel ones), otherwise
it will return as soon as serial tasks are finished (if any)